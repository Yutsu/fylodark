const form = document.getElementById('form');
const small = document.getElementById('small');

const navbar = document.querySelector('.navbar');
const info = document.querySelector('.info');
const productive = document.querySelector('.productive');
const footer = document.querySelector('.footer-access');

const logo = document.querySelector('.navbar-logo');
const menu = document.querySelector('.navbar-menu');
const card = document.querySelectorAll('.card');
const productiveImg = document.querySelector('.productive-img');
const productiveText = document.querySelector('.productive-text');

const formSubmit = (e) => {
    e.preventDefault();
    let emailValue = form['email'].value;
    if(!emailValue) {
        small.innerHTML ="Email empty";
    }
    else if(!validateEmail(emailValue)){
        small.innerHTML ="Email invalid";
    }
    else {
        small.innerHTML ="";
    }
}
const validateEmail = (email) => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

const gsapEvent = () => {
    let TL = gsap.timeline({
        scrollTrigger: { trigger: navbar }
    });
    TL
    .from(logo, 1, {x: -300, opacity: 0})
    .from(menu, 1, {x: 300, opacity: 0 },"-=0.9")

    let TLinfo = gsap.timeline({
        scrollTrigger: { trigger: info }
    });
    TLinfo
    .from(card, 1.5, {y: 300, opacity: 0 })

    let TLproduct = gsap.timeline({
        scrollTrigger: { trigger: productive }
    });
    TLproduct
    .from(productiveImg, 1.5, {x: -300, opacity: 0})
    .from(productiveText, 1.5, {x: 300, opacity: 0 },"-=1.5")

    let TLfooter = gsap.timeline({
        scrollTrigger: { trigger: footer }
    });
    TLfooter
    .from(footer, 3, {opacity: 0});
}

const app = () => {
    form.addEventListener('submit', formSubmit);
    gsapEvent();
}
app();